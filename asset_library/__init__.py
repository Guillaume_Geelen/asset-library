import os
import tkinter as tk

try :
    from PIL import Image, ImageTk
except :
    import pip
    pip.main(["install", "pillow"])
    from PIL import Image, ImageTk



class Asset :
    def __init__(self, app, path: str) :
        self.app = app
        self.path = path    
        self.tags = []  
        self.container = None
        

    def draw(self, master: tk.Widget, grid_pos = [0, 0]) :
        self.container = tk.Frame(master)
        self.container.grid()
        txt = self.path
        label = tk.Label(self.container, text=txt)
        label.pack()



class Asset_Library :
    def __init__(self) -> None:
        self.wn_root = tk.Tk()
        self.wn_root.title('Asset Library')

        self.assets_paths = [r"C:\Users\Guillaume Geelen\Documents\Guillaume fond\textures\Cuir"]
        self.assets = []

        self.assets_container = tk.Frame(self.wn_root)
        self.assets_container.pack()

        self.add_assets(self.assets_paths[0])
        
        self.wn_root.mainloop()


    def add_asset(self, asset_path: str) :
        new_asset = Asset(self, asset_path)
        new_asset.draw(self.assets_container)
        self.assets.append(new_asset)
        return new_asset


    def add_assets(self, path: str) :
        for item in os.listdir(path) :
            item_path = path + "\\" + item
            self.add_asset(item_path)


def main() :
    #start the app
    os.system('cls')
    print("\n___Asset_Library___\n\n".upper())

    app = Asset_Library()

    print("\n\n___END___\n")

if __name__ == "__main__" :
    main()